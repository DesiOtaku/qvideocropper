// Copyright 2023; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QDebug>
#include <QMimeData>
#include <QFileDialog>
#include <QDir>
#include <QSplitter>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    m_vidWid = new QVideoWidget();
    m_vidWid->setMinimumHeight(480);

    m_medPlayer = new QMediaPlayer();
    m_medPlayer->setVideoOutput(m_vidWid);

    m_scrubber = new ThumbnailScrubber();

    QSplitter *splitter = new QSplitter();
    splitter->setOrientation(Qt::Vertical);
    splitter->addWidget(m_vidWid);
    splitter->addWidget(m_scrubber);
    splitter->setChildrenCollapsible(false);

    ui->verticalLayout->addWidget(splitter);

    connect(ui->actionOpen, SIGNAL(triggered()), this, SLOT(handleOpenFile()));
    connect(ui->actionSave, SIGNAL(triggered()), this, SLOT(handleSave()));
    connect(m_medPlayer, SIGNAL(durationChanged(qint64)),m_scrubber,SLOT(handleNewDuration(qint64)));
    connect(m_medPlayer, SIGNAL(currentMediaChanged(QMediaContent)),m_scrubber,SLOT(handleNewFile(QMediaContent)));
    connect(m_scrubber,SIGNAL(newHoverValue(qint64)),m_medPlayer,SLOT(setPosition(qint64)));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::handlePosChangedByPlayer(qint64 duration)
{
    qDebug()<<duration;
}

void MainWindow::handlePosChangedByUser(qint64 duration)
{
    qDebug()<<duration;
}

void MainWindow::handleOpenFile()
{
    QString result = QFileDialog::getOpenFileName(this,"Open File", QDir::homePath());
    QUrl newFileUrl = QUrl::fromLocalFile(result);
    m_medPlayer->setMedia(newFileUrl);
    m_medPlayer->pause();
}

void MainWindow::handleSave()
{

}

void MainWindow::dragEnterEvent(QDragEnterEvent *event)
{
    if (event->mimeData()->hasUrls())
        event->acceptProposedAction();
}

void MainWindow::dropEvent(QDropEvent *event)
{
    m_medPlayer->setMedia(event->mimeData()->urls().first());
    m_medPlayer->pause();
    event->acceptProposedAction();
}
