// Copyright 2023; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

#ifndef THUMBNAILSCRUBBER_H
#define THUMBNAILSCRUBBER_H

#include <QGraphicsScene>
#include <QObject>
#include <QWidget>
#include <QGraphicsView>
#include <QMediaContent>
#include <QResizeEvent>
#include <QMouseEvent>
#include <QGraphicsLineItem>
#include <QPen>

class ThumbnailScrubber : public QGraphicsView
{
    Q_OBJECT
public:
    ThumbnailScrubber();
    void resizeEvent(QResizeEvent *event);
    void mouseMoveEvent(QMouseEvent *event);

signals:
    void newHoverValue(qint64 newDuration);

public slots:
    void handleNewFile(QMediaContent newFile);
    void handleNewDuration(qint64 newDuration);


private:
    void makeNewThumbnails();
    QPen makePen();
    QGraphicsScene *m_scene;
    QString m_fileURL;
    qint64 m_duration;
    QVector<QPixmap> m_images;
    QGraphicsLineItem *m_line;
};

#endif // THUMBNAILSCRUBBER_H
