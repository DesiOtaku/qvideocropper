// Copyright 2023; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

#include <QMediaPlayer>
#include <QVideoWidget>
#include <QDragEnterEvent>

#include "thumbnailscrubber.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    void dragEnterEvent(QDragEnterEvent *event);
    void dropEvent(QDropEvent *event);

public slots:
    void handlePosChangedByPlayer(qint64 duration);
    void handlePosChangedByUser(qint64 duration);
    void handleOpenFile();
    void handleSave();

private:
    Ui::MainWindow *ui;
    QVideoWidget *m_vidWid;
    QMediaPlayer *m_medPlayer;
    ThumbnailScrubber *m_scrubber;
};
#endif // MAINWINDOW_H
