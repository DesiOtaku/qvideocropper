// Copyright 2023; Tej A. Shah
// Licensed under GPLv3+
// Refer to the LICENSE file for details

#include "thumbnailscrubber.h"

#include <QProcess>
#include <QDebug>
#include <QDir>
#include <QGraphicsPixmapItem>
#include <QProgressDialog>

ThumbnailScrubber::ThumbnailScrubber()
{
    m_scene = new QGraphicsScene();
    this->setScene(m_scene);
    m_fileURL = "";
    m_duration = -1;
    this->setMinimumHeight(64);
    this->setAttribute(Qt::WA_Hover);
    this->setMouseTracking(true);
    m_line = new QGraphicsLineItem(0,0,0,0);
    m_line->setPen(makePen());
}

void ThumbnailScrubber::resizeEvent(QResizeEvent *event)
{
    if(m_images.length() > 0) {
        int imgHeight = m_images.first().height();
        int imgWidth = m_images.first().width();
        int rowLength = imgWidth * m_images.length();
        this->fitInView(0,0,rowLength,imgHeight,Qt::KeepAspectRatio);
    }
    event->accept();
}

void ThumbnailScrubber::mouseMoveEvent(QMouseEvent *event)
{
    if(m_images.length() > 0) {
        QPointF scenePos = this->mapToScene(event->pos());
        qreal originalx = scenePos.x();
        int imgWidth = m_images.first().width();
        int rowLength = imgWidth * m_images.length();
        int penWidth = m_line->pen().width()/2;
        if(scenePos.x()< penWidth) {
            scenePos.setX(m_line->pen().width()+1);
        }
        else if(scenePos.x() > (rowLength -penWidth )) {
            scenePos.setX(rowLength -penWidth-1);
        }
        m_line->setLine(scenePos.x(),0,scenePos.x(),m_images.first().height());
        qreal ratio = originalx / rowLength;
        qint64 hoveredMils =(qint64) (ratio * m_duration);
        qDebug()<<hoveredMils;
        emit newHoverValue(hoveredMils);
    }
    event->accept();
}



void ThumbnailScrubber::handleNewFile(QMediaContent newFile)
{
    m_fileURL= newFile.request().url().toString();
    m_fileURL = m_fileURL.replace("file://","");
    makeNewThumbnails();
}

void ThumbnailScrubber::handleNewDuration(qint64 newDuration)
{
    m_duration= newDuration;
    makeNewThumbnails();
}

void ThumbnailScrubber::makeNewThumbnails()
{
    if((m_fileURL.length() < 2) || (m_duration < 2)) {
        return; //so sue me
    }



    QString imgOutput = QDir::tempPath() + "/ffmpegOutput.png";
    m_images.clear();

    int secondsDur = m_duration / 1000;
    QProgressDialog progress("Reading Video", QString(), 0, secondsDur, this);
    progress.setWindowModality(Qt::WindowModal);


    for(int i=1;i<secondsDur;i+= (secondsDur/10)) {
        QProcess ffmpeg;
        QStringList args;
        int hours = i/3600;
        int minutes = (i - (hours*3600)) / 60;
        int seconds = i %60;
        args<<"-ss";
        args<< QString::number(hours) + ":" + QString::number(minutes) + ":" + QString::number(seconds);
        args<<"-i";
        args<<m_fileURL;
        args<<"-frames:v";
        args<<"1";
        args<<"-y";
        args<<imgOutput;
        ffmpeg.setArguments(args);
        ffmpeg.start("/usr/bin/ffmpeg",args);
        ffmpeg.waitForFinished();
        QPixmap readImage(imgOutput);
        m_images.append(readImage);
        progress.setValue(i);
    }
    progress.setValue(secondsDur);

    this->scene()->clear();
    int imgHeight = m_images.first().height();
    int imgWidth = m_images.first().width();
    int rowLength = imgWidth * m_images.length();
    for(int i=0;i<m_images.length();i++) {
        QGraphicsPixmapItem *imgItem = this->scene()->addPixmap(m_images.at(i));
        imgItem->setX(i*imgWidth);
    }
    this->scene()->addItem(m_line);
    m_line->setPen(makePen());
    this->fitInView(0,0,rowLength,imgHeight,Qt::KeepAspectRatio);
}

QPen ThumbnailScrubber::makePen()
{
    QPen returnMe;
    returnMe.setColor(QColor(0,0,255,175));
    if(m_images.length() > 1) {
        returnMe.setWidth(m_images.first().width() / 25);
    }
    else {
        returnMe.setWidth(50);
    }
    return returnMe;
}
